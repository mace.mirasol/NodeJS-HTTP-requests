const axios = require('axios');
const func = require('./functions');
const province = 'Iloilo';
const municipality = 'Iloilo City';
let url =
  'https://member.lazada.com.ph/locationtree/api/getSubAddressList?countryCode=PH';
const addMe = '&addressId=';
let addressId = '';

// Async - Await function
const getRequest = async () => {
  try {
    let response,
      location = '';

    for (let i = 0; i < 3; i++) {
      response = await axios.get(url + addressId);
      i === 0 ? (location = province) : (location = municipality);
      i < 2
        ? (addressId = addMe + func.getId(response.data.module, location))
        : func.convertToCsv(response.data.module, 'async_await.csv');
    }
  } catch (error) {
    console.error(error);
  }
};

// Function call
getRequest();
