// Retreiving specific addressId
const getId = function (obj, location) {
  return obj.find(loc => loc.name === location).id;
};

// Converting json to .csv
const fileSystem = require('fs');
const {format} = require('@fast-csv/format');

const convertToCsv = (json, filename) => {
  console.log(json);
  const ws = fileSystem.createWriteStream('../csv/' + filename);
  const csvStream = format({headers: true});

  csvStream.pipe(ws).on('end', () => process.exit());

  for (let row of json) {
    csvStream.write({id: row.id, name: row.name, parentId: row.parentId});
  }

  csvStream.end();
};

module.exports = {getId, convertToCsv};
