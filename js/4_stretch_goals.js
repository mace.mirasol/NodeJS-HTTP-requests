const axios = require('axios');
const func = require('./functions');
const province = 'Aklan';
let url =
  'https://member.lazada.com.ph/locationtree/api/getSubAddressList?countryCode=PH';
const addMe = '&addressId=';
// let addressId = '';
const barangays = [];

// Async - Await function
const getRequest = async () => {
  let response = await axios.get(url);
  console.log(response.data);
  try {
    response.data.module.map(async prov => {
      response = await axios.get(url + addMe + prov.id);
      // console.log(url + addMe + prov.id);
      barangays.push(response.data.module);
      // try {
      //   response.data.module.map(async mun => {
      //     response = await axios.get(url + addMe + mun.id);
      //     barangays.push(response.data.module);
      //   });
      // } catch (e) {
      //   console.log(e);
      // }
    });
    console.log(barangays);
    // func.convertToCsv(barangays, 'barangays.csv');
  } catch (e) {
    console.log(e);
  }
};

// Function call
getRequest();

// console.log(barangays);
