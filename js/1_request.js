const request = require('request');
const func = require('./functions');
const province = 'Iloilo';
const municipality = 'Iloilo City';
let url =
  'https://member.lazada.com.ph/locationtree/api/getSubAddressList?countryCode=PH&addressId=';
let addressId = '';

const getRequest = (url, cb, data) => {
  request(
    {
      method: 'GET',
      url: url,
      headers: {
        'Content-Type': 'application/json',
        dataType: 'json',
      },
    },
    (error, response, body) => {
      if (cb) {
        cb(error, JSON.parse(body));
      }
    }
  );
};

// First Data Fetch
getRequest(url, (error, data) => {
  if (error) throw error;
  addressId = func.getId(data.module, province);

  // Second Data Fetch
  getRequest(url + addressId, (error, data) => {
    if (error) throw error;
    addressId = func.getId(data.module, municipality);

    // Third Data Fetch
    getRequest(url + addressId, (error, data) => {
      if (error) throw error;
      func.convertToCsv(data.module, 'request.csv');
    });
  });
});
