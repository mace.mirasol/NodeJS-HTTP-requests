const axios = require('axios');
const func = require('./functions');
const province = 'Iloilo';
const municipality = 'Iloilo City';
let url =
  'https://member.lazada.com.ph/locationtree/api/getSubAddressList?countryCode=PH';
const addMe = '&addressId=';
let addressId = '';

// First Data Fetch
axios
  .get(url)
  .then(({data}) => {
    addressId = addMe + func.getId(data.module, province);
  })
  .catch(error => console.log(error))

  // Second Data Fetch
  .then(() =>
    axios.get(url + addressId).then(({data}) => {
      addressId = addMe + func.getId(data.module, municipality);
    })
  )
  .catch(error => console.log(error))

  // Third Data Fetch
  .then(() =>
    axios.get(url + addressId).then(({data}) => {
      func.convertToCsv(data.module, 'promise.csv');
    })
  )
  .catch(error => console.log(error));
